//
//  RLRhinoBase.m
//  RhinoLib
//
//  Created by Alex Rudyak on 10/23/14.
//  Copyright (c) 2014 Alex Rudyak. All rights reserved.
//

#import "RLRhinoBase.h"

@implementation RLRhinoBase
static float Speed = 10.1f;

- (void)run
{
    // run forward
}

- (void)runWithSpeed:(float)speed
{
    Speed = speed;
    [self run];
}

@end
