//
//  RLRhinoBase.h
//  RhinoLib
//
//  Created by Alex Rudyak on 10/23/14.
//  Copyright (c) 2014 Alex Rudyak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RLRhinoBase : NSObject

- (void)run;
- (void)runWithSpeed:(float)speed;

@end
