# RhinoLib

[![CI Status](http://img.shields.io/travis/Alex Rudyak/RhinoLib.svg?style=flat)](https://travis-ci.org/Alex Rudyak/RhinoLib)
[![Version](https://img.shields.io/cocoapods/v/RhinoLib.svg?style=flat)](http://cocoadocs.org/docsets/RhinoLib)
[![License](https://img.shields.io/cocoapods/l/RhinoLib.svg?style=flat)](http://cocoadocs.org/docsets/RhinoLib)
[![Platform](https://img.shields.io/cocoapods/p/RhinoLib.svg?style=flat)](http://cocoadocs.org/docsets/RhinoLib)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RhinoLib is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "RhinoLib"

## Author

Alex Rudyak, aliaksandr.rudziak@instinctools.ru

## License

RhinoLib is available under the MIT license. See the LICENSE file for more info.

