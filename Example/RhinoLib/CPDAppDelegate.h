//
//  RLAppDelegate.h
//  RhinoLib
//
//  Created by CocoaPods on 10/23/2014.
//  Copyright (c) 2014 Alex Rudyak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
