//
//  main.m
//  RhinoLib
//
//  Created by Alex Rudyak on 10/23/2014.
//  Copyright (c) 2014 Alex Rudyak. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RLAppDelegate class]));
    }
}
